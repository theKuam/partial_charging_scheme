import os
import random
import numpy as np
import gc
import itertools as it
import timeit

INPUT_DIRECTORY = "dulieu/20200713_dataset_final/"
C_RATE = 1.05
CRATE = [5]
E = [13500, 27000, 54000, 81000]
file_names = []
n_seeds = 1
sensors = []
population = None
n = 0
n_gene = 0
distance = None

POP_SIZE = 251
ALPHA = 0.8
BETA = 0.2
GAMMA = 0.1
lower_bound = 0
upper_bound = 0
N_GENERATIONS_PHASE_2 = 300
N_GENERATIONS_PHASE_3 = 100
N_GENERATIONS_BI_LEVEL = 10
N_GENERATIONS_UPPER_LEVEL = 10
N_GENERATIONS_LOWER_LEVEL = 10
CROSSOVER_RATE = 0.75
MUTATION_RATE = 0.1
epsilon = 1e-6
nm = 5.0
nc = 2.0
nc2 = 2.0
theta = 0.5

SCALE = 1.5
total_T = 20000
E_MC = 108000
U = 5
E_MAX = 10800
E_MIN = 540
P_M = 1
V = 5


class Sensor:
    x = y = p = remain_energy = charging_ratio = charging_probability = 0
    is_base_station = False

    def __init__(self, *args):
        if len(args) == 2:
            self.x = args[0]
            self.y = args[1]
            self.is_base_station = True
        else:
            self.x = args[0]
            self.y = args[1]
            self.p = args[2]
            self.remain_energy = args[3]


class Individual:
    path = []
    encoded_path = []
    charging_times = []
    n_charged_nodes = n_dead_nodes = fitness = charging_t = traveling_t = first_dead_t = average_remain_e = 0

    def __init__(self, *args):
        if len(args) == 1:
            self.path = args[0].path.copy()
            self.encoded_path = args[0].encoded_path.copy()
            self.charging_times = args[0].charging_times.copy()
            self.n_dead_nodes = args[0].n_dead_nodes
            self.fitness = args[0].fitness
            self.charging_t = args[0].charging_t
            self.traveling_t = args[0].traveling_t
            self.first_dead_t = args[0].first_dead_t
            self.n_charged_nodes = args[0].n_charged_nodes


class Population:
    individuals = []
    best = []

    def __init__(self, *args):
        if len(args) == 1:
            self.individuals = args[0].individuals
            self.best = args[0].best


def get_file_names(dir):
    if os.path.isdir(dir):
        for file in os.listdir(dir):
            path = dir + file
            if not os.path.isfile(path):
                path += "/"
                get_file_names(path)
            else:
                file_names.append(path)
    else:
        print("Is not a directory.")


def read_file(file_name_index, seed_value, prate):
    print(file_names[file_name_index])
    random.seed(seed_value)
    file_name = file_names[file_name_index]
    file_stream = open(file_name, "r")
    file_lines = file_stream.readlines()
    global n  # number of sensors
    n = 0
    p_max = 0  # the greatest power consumption of a sensor
    for line in file_lines:
        line_split = line.split(" ")
        x = float(line_split[0])  # x-coordinate of the base station / a sensor
        y = float(line_split[1])  # y-coordinate of the base station / a sensor
        if (n == 0) and (not sensors):
            sensors.append(Sensor(x, y))
        else:
            p = float(line_split[2]) * prate  # power consumption of a sensor
            remain_energy = float(line_split[3])  # remain energy of a sensor
            if p > p_max:
                p_max = p
            sensors.append(Sensor(x, y, p, remain_energy))
            n += 1
    file_stream.close()
    # calculate distance between two arbitrary sensors
    global distance
    distance = [[0 for x in range(0, n + 1)] for y in range(0, n + 1)]
    for k in range(0, n + 1):
        for h in range(0, n + 1):
            if k == h:
                distance[k][h] = np.inf
            else:
                distance[k][h] = np.sqrt(
                    np.power(sensors[k].x - sensors[h].y, 2) + np.power(sensors[k].y - sensors[h].y, 2))

    # calculate the charging ratio
    for sensor in sensors:
        if not sensor.is_base_station:
            sensor.charging_ratio = sensor.p / p_max / C_RATE


def phase_1():
    # pseudo_fuzzy()
    # random_charging_prob()
    all_charging_prob()
    # fuzzy()


def pseudo_fuzzy():
    global upper_bound, lower_bound
    upper_bound = 0
    lower_bound = 0
    for sensor in sensors:
        if sensor.is_base_station or sensor.remain_energy < E_MIN:
            continue
        if (sensor.remain_energy - E_MIN) / sensor.p >= T:
            sensor.charging_probability = BETA + (1 - BETA) * sensor.charging_ratio / 5
        else:
            if sensor.remain_energy < E_MIN:
                sensor.charging_probability = 0
            elif sensor.remain_energy + E_MAX - E_MIN - sensor.p * T < E_MIN:
                sensor.charging_probability = GAMMA * sensor.charging_ratio
                lower_bound += 1
            else:
                sensor.charging_probability = ALPHA + (1 - ALPHA) * sensor.charging_ratio
            if sensor.remain_energy - sensor.p * T < E_MIN:
                upper_bound += 1


def random_charging_prob():
    for sensor in sensors:
        if sensor.is_base_station or sensor.remain_energy < E_MIN:
            continue
        sensor.charging_probability = .5


def all_charging_prob():
    for sensor in sensors:
        if sensor.is_base_station or sensor.remain_energy < E_MIN:
            continue
        sensor.charging_probability = 1


def insertion_sort(array):
    for i in range(0, len(array)):
        for j in range(i + 1, len(array)):
            if array[i][0] < array[j][0]:
                array[i], array[j] = array[j], array[i]
    return array


def decode(encoded_path):
    ran_array = []  # there are two data materials in one gene: the random key and the sensor id
    charging_probs = []
    for i in range(0, n):
        charging_probs.append(sensors[i].charging_probability)
        if encoded_path[i] < sensors[i].charging_probability:
            gene = [0, 0.0]
            gene[0] = encoded_path[i]  # store the random key
            gene[1] = i  # store the sensor id
            ran_array.append(gene)
    ran_array = insertion_sort(ran_array)
    # return the result_path
    result_path = [0 for x in range(0, len(ran_array) + 1)]
    for i in range(1, len(ran_array)):
        result_path[i] = ran_array[i][1]
    result_path[len(ran_array)] = 0
    if len(result_path) == 1 or len(result_path) == 2:
        result_path = [0, np.argmax(charging_probs), 0]
    return result_path


def init_fuzzy(individual):
    path_size = len(individual.path)
    individual.charging_times = [0 for x in range(0, path_size)]
    traveling_time = 0
    charging_ratio_sum = 0
    for i in range(1, path_size - 1):
        traveling_time += distance[i][i - 1] / V
        charging_ratio_sum += sensors[individual.path[i]].charging_ratio
    traveling_time += distance[path_size - 2][0] / V
    individual.traveling_t = traveling_time
    individual.charging_t = (E_MC - individual.traveling_t * P_M) / U
    for i in range(1, path_size - 1):
        individual.charging_times[i] = sensors[individual.path[
            i]].charging_ratio / charging_ratio_sum * individual.charging_t
    individual.charging_times[path_size - 1] = 0
    return individual


def init_fitness(individual):
    path_size = len(individual.path)
    traveling_time = charging_time = n_dead = 0
    first_dead_time = np.inf
    temp_individual = Individual(individual)
    is_dead_1 = [False for x in
                 range(0, n + 1)]  # the decision variable for the dead node when the MC arrive at that node.
    is_dead_2 = [False for x in
                 range(0, n + 1)]  # the decision variable for the dead node after the MC finished the charging
    # round.
    dead_times = [np.inf for x in range(0, n + 1)]
    is_charged = [False for x in range(0, n + 1)]

    # Check the remain energy of each sensor when the MC had arrived
    for i in range(1, path_size - 1):
        current_index = individual.path[i]
        previous_index = individual.path[i - 1]
        traveling_time += distance[current_index][previous_index] / V
        if traveling_time + charging_time > T:
            traveling_time = T - charging_time
            break
        is_charged[current_index] = True
        # If the remain energy of a sensor is smaller than E_min, the sensor is dead
        if sensors[current_index].remain_energy - (traveling_time + charging_time) * sensors[current_index].p < E_MIN:
            is_dead_1[current_index] = True
            dead_times[current_index] = (sensors[current_index].remain_energy - E_MIN) / sensors[current_index].p
        # after the sensor is charged, if the energy of the sensor is greater than E_MAX, we have to reduce the
        # charging time.
        if sensors[current_index].remain_energy - (traveling_time + charging_time) * sensors[current_index].p + \
                individual.charging_times[i] * (U - sensors[current_index].p) > E_MAX:
            # calculate the energy different
            energy_differ = - E_MAX + sensors[current_index].remain_energy - (traveling_time + charging_time) * sensors[
                current_index].p + individual.charging_times[i] * (U - sensors[current_index].p)
            # calculate the charging time different
            charging_time_differ = energy_differ / np.abs(U - sensors[current_index].p)
            # reduce the charging time in the individual
            individual.charging_t -= charging_time_differ
            individual.charging_times[i] -= charging_time_differ
        if is_dead_1[current_index]:
            individual.charging_times[i] = 0
            continue
        else:
            charging_time += individual.charging_times[i]
            if traveling_time + charging_time > T:
                time_differ = traveling_time + charging_time - T
                individual.charging_t -= time_differ
                individual.charging_times[i] -= time_differ
                charging_time -= time_differ
                break
        if i == path_size - 2:
            traveling_time += distance[current_index][0] / V
    for i in (x for x in range(1, path_size - 1) if is_charged[individual.path[x]]):
        current_index = individual.path[i]
        e_depot = sensors[current_index].remain_energy - T * sensors[current_index].p + individual.charging_times[i] * U
        if e_depot < E_MIN:
            is_dead_2[current_index] = True
            if dead_times[current_index] == np.inf:
                dead_times[current_index] = (sensors[current_index].remain_energy + individual.charging_times[
                    i] * U - E_MIN) / sensors[current_index].p
    for i in range(1, n + 1):
        if is_charged[i]: continue
        if sensors[i].remain_energy - T * sensors[i].p < E_MIN:
            is_dead_2[i] = 1
            dead_times[i] = (sensors[i].remain_energy - E_MIN) / sensors[i].p
    for i in range(1, n + 1):
        if is_dead_1[i] or is_dead_2[i]:
            n_dead += 1
        if dead_times[i] < first_dead_time:
            first_dead_time = dead_times[i]

    remain_sum = 0
    for i in range(1, n + 1):
        remain_sum += sensors[i].remain_energy - sensors[i].p * T
        if i in individual.path:
            remain_sum += individual.charging_times[individual.path.index(i)] * U
    individual.average_remain_e = E_MAX - remain_sum/n
    individual.n_dead_nodes = n_dead
    individual.n_charged_nodes = is_charged.count(True)
    individual.first_dead_t = first_dead_time
    individual.traveling_t = traveling_time
    individual.charging_t = charging_time

    individual.fitness = (individual.n_dead_nodes + np.abs(P_M * individual.traveling_t) / E_MC)

    individual.charging_times = temp_individual.charging_times
    individual.charging_t = temp_individual.charging_t
    if not population.best:
        population.best = individual
    elif individual.fitness < population.best.fitness:
        population.best = individual
    return individual


def random_key_encoding():
    individual = Individual()
    encoded_path = [0 for x in range(n + 1)]
    for i in range(1, n):
        random_value = np.random.rand()
        encoded_path[i] = random_value
    encoded_path[n] = 0
    individual.encoded_path = encoded_path
    individual.path = decode(individual.encoded_path)
    individual = init_fuzzy(individual)
    individual = init_fitness(individual)
    return individual


def population_init_phase_2():
    global population
    population = Population()
    population.individuals = [None for x in range(POP_SIZE)]
    for i in range(0, POP_SIZE):
        individual = random_key_encoding()
        # individual = permutation_encoding()
        population.individuals[i] = individual


def sbx(parent_1, parent_2):
    random_x = np.random.uniform(epsilon, 1 - epsilon)
    if random_x <= 0.5:
        beta = np.power(float(2.0 * random_x), float(1 / (nc + 1)))
    else:
        beta = np.power(float(0.5 / (1 - random_x)), float(1 / (nc + 1)))
    child_1 = Individual()
    child_2 = Individual()
    child_1.encoded_path = [0 for x in range(0, n + 2)]
    child_2.encoded_path = [0 for x in range(0, n + 2)]
    for i in range(1, n + 1):
        child_1.encoded_path[i] = (parent_1.encoded_path[i] * (1 + beta) + parent_2.encoded_path[i] * (1 - beta)) * 0.5
        child_2.encoded_path[i] = (parent_2.encoded_path[i] * (1 + beta) + parent_1.encoded_path[i] * (1 - beta)) * 0.5
    child_1.encoded_path[n + 1] = 0
    child_2.encoded_path[n + 1] = 0
    child_1.path = decode(child_1.encoded_path)
    child_2.path = decode(child_2.encoded_path)
    child_1 = init_fuzzy(child_1)
    child_2 = init_fuzzy(child_2)
    child_1 = init_fitness(child_1)
    child_2 = init_fitness(child_2)
    return child_1, child_2


def polymute(child):
    for i in range(1, n + 1):
        random_m = np.random.uniform(epsilon, 1 - epsilon)
        if random_m <= 0.5:
            phi = np.power(2 * random_m, 1 / (nm + 1)) - 1
            child.encoded_path[i] = child.encoded_path[i] * (phi + 1)
        else:
            phi = 1 - np.power(2 * (1 - random_m), 1 / (nm + 1))
            child.encoded_path[i] = child.encoded_path[i] + phi * (1 - child.encoded_path[i])
    child.path = decode(child.encoded_path)
    child = init_fuzzy(child)
    child = init_fitness(child)
    return child


def reproduction_phase_2():
    for i in range(0, int(POP_SIZE / 2)):
        mom_1, mom_2 = np.random.randint(0, POP_SIZE - 1, 2)
        if population.individuals[mom_2].fitness < population.individuals[mom_1].fitness:
            mom_1 = mom_2
        dad_1 = mom_1
        while dad_1 == mom_1:
            dad_1, dad_2 = np.random.randint(0, POP_SIZE - 1, 2)
            if population.individuals[dad_2].fitness < population.individuals[dad_1].fitness:
                dad_1 = dad_2
        parent_1 = population.individuals[dad_1]
        parent_2 = population.individuals[mom_1]
        child_1 = Individual(parent_1)
        child_2 = Individual(parent_2)
        if np.random.rand() <= CROSSOVER_RATE:
            child_1, child_2 = sbx(parent_1, parent_2)
            if np.random.rand() <= MUTATION_RATE:
                child_1 = polymute(child_1)
            if np.random.rand() <= MUTATION_RATE:
                child_2 = polymute(child_2)
        population.individuals.append(child_1)
        population.individuals.append(child_2)


def charging_path_solving():
    population_init_phase_2()
    for gen in range(0, N_GENERATIONS_PHASE_2):
        print("Phase 2 gen " + str(gen))
        best_index = 0
        for i in range(1, POP_SIZE):
            if population.individuals[i].fitness < population.individuals[best_index].fitness:
                best_index = i
        population.best = population.individuals[best_index]
        reproduction_phase_2()
        population.individuals.sort(key=lambda x: x.fitness)
    return population.best


def population_init_phase_3():
    charging_time = np.min(np.array([E_MC - population.best.traveling_t * P_M, T - population.best.traveling_t]))
    if charging_time < 0:
        charging_time = 0

    population.best.charging_t = charging_time

    for i in range(0, len(population.individuals)):
        population.individuals[i] = population.best
        remain_time = charging_time
        if i == 0:
            continue
        population.individuals[i] = calculate_charging_time(population.individuals[i], remain_time)


def calculate_charging_time(individual, remain_time):
    for j in range(1, len(individual.charging_times) - 1):
        sensor = sensors[individual.path[j]]
        upper_bound = remain_time
        if upper_bound > (E_MAX - E_MIN) / (U - sensor.p):
            upper_bound = (E_MAX - E_MIN) / (U - sensor.p)
        if upper_bound < 0:
            upper_bound = 0
        random_t = np.random.uniform(0, upper_bound)
        individual.charging_times[j] = random_t
        remain_time -= random_t
    return init_fitness(individual)


def spah(parent_1, parent_2):
    child_1 = parent_1
    child_2 = parent_2
    # select a random cut point for spah
    cut_point = np.random.randint(0, len(parent_1.path) - 2)
    # calculate the actual charging time
    charging_time = (E_MC - parent_1.traveling_t * P_M) / U
    spx_beta = np.random.uniform(-0.5, 0.5)
    # swap right gene segments from the cut point
    for i in range(cut_point + 1, len(parent_1.path) - 1):
        child_1.charging_times[i] = parent_2.charging_times[i]
        child_2.charging_times[i] = parent_1.charging_times[i]
    # calculate genes at the cut point
    child_1.charging_times[cut_point] = np.abs(
        (1 - spx_beta) * parent_1.charging_times[cut_point] + spx_beta * parent_2.charging_times[cut_point])
    child_2.charging_times[cut_point] = np.abs(
        spx_beta * parent_1.charging_times[cut_point] + (1 - spx_beta) * parent_2.charging_times[cut_point])
    charging_time_1 = charging_time_2 = 0
    # modify the unsatisfied charging times
    for i in range(1, len(parent_1.path) - 1):
        charging_time_1 += child_1.charging_times[i]
        charging_time_2 += child_2.charging_times[i]

    if charging_time_1 > charging_time:
        t_ratio = charging_time_1 / charging_time
        for i in range(1, len(child_1.charging_times) - 1):
            child_1.charging_times[i] /= t_ratio
    else:
        t_differ = charging_time - charging_time_1
        for i in range(1, len(child_1.charging_times) - 1):
            random_t = np.random.uniform(0,
                                         (E_MAX - E_MIN) / (U - sensors[child_1.path[i]].p) - child_1.charging_times[i])
            if random_t > t_differ:
                random_t = t_differ
            child_1.charging_times[i] += random_t
            t_differ -= random_t

    if charging_time_2 > charging_time:
        t_ratio = charging_time_2 / charging_time
        for i in range(1, len(child_2.charging_times) - 1):
            child_2.charging_times[i] /= t_ratio
    else:
        t_differ = charging_time - charging_time_2
        for i in range(1, len(child_2.charging_times) - 1):
            random_t = np.random.uniform(0,
                                         (E_MAX - E_MIN) / (U - sensors[child_2.path[i]].p) - child_2.charging_times[i])
            if random_t > t_differ:
                random_t = t_differ
            child_2.charging_times[i] += random_t
            t_differ -= random_t

    child_1 = init_fitness(child_1)
    child_2 = init_fitness(child_2)
    return child_1, child_2


def mut_2(child):
    duplicate_count = 0
    i = np.random.randint(0, len(child.charging_times) - 2)
    j = np.random.randint(0, len(child.charging_times) - 2)
    while i == j or i == 0:
        duplicate_count += 1
        j = np.random.randint(0, len(child.charging_times) - 2)
        if duplicate_count == 10:
            break
    mod_1 = np.random.uniform(0, (E_MAX - E_MIN) / (U - sensors[child.path[i]].p) - child.charging_times[i])
    mod_2 = np.random.uniform(0, child.charging_times[j])
    if mod_1 > mod_2:
        mod_1 = mod_2
    child.charging_times[i] += mod_1
    child.charging_times[j] -= mod_1
    child = init_fitness(child)
    return child


def reproduction_phase_3():
    for i in range(0, int(POP_SIZE / 2)):
        mom_1, dad_1 = np.random.randint(0, POP_SIZE - 1, 2)
        while dad_1 == mom_1:
            dad_1 = np.random.randint(0, POP_SIZE - 1)
        parent_1 = population.individuals[dad_1]
        parent_2 = population.individuals[mom_1]
        child_1 = Individual(parent_1)
        child_2 = Individual(parent_2)
        if np.random.rand() <= CROSSOVER_RATE:
            child_1, child_2 = spah(parent_1, parent_2)
            if np.random.rand() <= MUTATION_RATE:
                child_1 = mut_2(child_1)
            if np.random.rand() <= MUTATION_RATE:
                child_2 = mut_2(child_2)
        population.individuals.append(child_1)
        population.individuals.append(child_2)


def charging_time_solving():
    population_init_phase_3()
    for gen in range(0, N_GENERATIONS_PHASE_3):
        print("Phase 3 Gen " + str(gen))
        for i in range(0, POP_SIZE):
            population.best = population.individuals[0]
        reproduction_phase_3()
        population.individuals.sort(key=lambda x: x.fitness)

    return population.best


def output(solution):
    pass


def phase_2():
    solution = charging_path_solving()
    # refine the solution
    # solution = output(solution)
    return solution


def phase_3():
    solution = charging_time_solving()
    # refine the solution
    # solution = output(solution)
    return solution


def check(solution):
    print("STT | "
          "Nang_luong_ban_dau | "
          "Cong_suat | "
          "Thoi_gian_cho_sac | "
          "Nang_luong_bat_dau_sac | "
          "Thoi_gian_sac | "
          "Nang_luong_sau_khi_sac | "
          "Nang_luong_T")
    traveling_time = charging_time = 0
    is_charged = [False for x in range(0, n + 2)]
    for i in range(1, len(solution.path) - 1):
        current_index = solution.path[i]
        previous_index = solution.path[i - 1]
        traveling_time += distance[current_index][previous_index] / V
        if traveling_time > T:
            break
        is_charged[current_index] = True
        remain = sensors[current_index].remain_energy - sensors[current_index].p * T + \
                 solution.charging_times[i] * U
        print(str(current_index) + "|"
              + str(sensors[current_index].remain_energy) + "|"
              + str(sensors[current_index].p) + "|"
              + str(traveling_time + charging_time) + "|"
              + str(
            sensors[current_index].remain_energy - sensors[current_index].p * (traveling_time + charging_time)) + "|"
              + str(solution.charging_times[i]) + "|"
              + str(sensors[current_index].remain_energy - sensors[current_index].p * (traveling_time + charging_time) +
                    solution.charging_times[i] * (U - sensors[current_index].p)) + "|"
              + str(remain))
        if sensors[current_index].remain_energy - sensors[current_index].p * (
                traveling_time + charging_time) <= remain <= E_MIN:
            print("        CHET")
        if remain >= E_MAX:
            sensors[current_index].remain_energy = E_MAX
        else:
            sensors[current_index].remain_energy = remain
        charging_time += solution.charging_times[i]

    for i in range(1, n + 1):
        if is_charged[i]:
            continue
        print(str(i) + "|"
              + str(sensors[i].remain_energy) + "|"
              + str(sensors[i].p) + "|"
              + str(sensors[i].remain_energy - sensors[i].p * T))
        if sensors[i].remain_energy - sensors[i].p * T < E_MIN:
            print("        CHET")
        sensors[i].remain_energy = sensors[i].remain_energy - sensors[i].p * T


def write_file(fileghi1, fileghi2, fileghi3, fileghi4, result, file_index, running_time, solution, c_rate):
    file_stream_1 = open(fileghi1, "a")
    file_stream_2 = open(fileghi2, "a")
    file_stream_3 = open(fileghi3, "a")
    file_stream_4 = open(fileghi4, "a")
    w_file_stream = open(result, "a")
    file_stream_1.write(str(solution.fitness) + "\n")
    file_stream_2.write(str(solution.traveling_t) + "\n")
    file_stream_3.write(str(lower_bound) + "\n")
    file_stream_4.write(str(upper_bound) + "\n")
    w_file_stream.write(str(file_names[file_index]) + " " + str(c_rate) + " "
                        + str(int(solution.n_dead_nodes)) + " "
                        + str(int(len(solution.path) - 2)) + " "
                        + str(solution.traveling_t * P_M) + " "
                        + str(solution.charging_t * U) + " "
                        + str(solution.charging_t) + " "
                        + str(running_time) + "\n")
    file_stream_1.close()
    file_stream_2.close()
    file_stream_3.close()
    file_stream_4.close()


def charging_path_solving_bi_level():
    initPopulation = Population(population)
    for i in range(0, N_GENERATIONS_UPPER_LEVEL):
        print("-UPPER LEVEL Gen " + str(i))
        reproduction_upper_level(initPopulation)
        initPopulation.individuals = sorted(initPopulation.individuals, key=lambda x: x.fitness)[:POP_SIZE]
    best_indviduals = initPopulation.individuals[:round(10 * POP_SIZE / 100)]
    return best_indviduals


def charging_time_solving_bi_level(individuals):
    for individual in individuals:
        charging_time = np.min(np.array([E_MC - individual.traveling_t * P_M, T - individual.traveling_t]))
        if charging_time < 0:
            charging_time = 0

        individual.charging_t = charging_time
        chargingTimePopulation = Population()
        chargingTimePopulation.individuals = [None for x in range(POP_SIZE)]
        for i in range(0, POP_SIZE):
            chargingTimePopulation.individuals[i] = Individual(calculate_charging_time(individual, charging_time))
        for i in range(0, N_GENERATIONS_LOWER_LEVEL):
            print("-LOWER LEVEL Gen " + str(i) + " Individual " + str(individuals.index(individual)))
            reproduction_lower_level(chargingTimePopulation)
            chargingTimePopulation.individuals = sorted(chargingTimePopulation.individuals, key=lambda x: x.fitness)[:POP_SIZE]
        individual = chargingTimePopulation.individuals[0]
    return individuals


def pmx(parent_1, parent_2):
    cut_point_1 = np.random.randint(1, len(parent_1.path) - 2)
    cut_point_2 = np.random.randint(1, len(parent_1.path) - 2)
    while cut_point_2 == cut_point_1:
        cut_point_2 = np.random.randint(1, len(parent_1.path) - 2)
    if cut_point_1 > cut_point_2:
        cut_point_1, cut_point_2 = cut_point_2, cut_point_1
    child_1 = Individual(parent_1)
    child_2 = Individual(parent_2)
    child_1.path[1:cut_point_1] = [0] * (cut_point_1 - 1)
    child_1.path[cut_point_2 + 1:(len(parent_1.path) - 1)] = [0] * (len(parent_1.path) - cut_point_2 - 2)
    child_2.path[1:cut_point_1] = [0] * (cut_point_1 - 1)
    child_2.path[cut_point_2 + 1:(len(parent_1.path) - 1)] = [0] * (len(parent_1.path) - cut_point_2 - 2)
    swath_1 = child_1.path[cut_point_1:cut_point_2 + 1]
    swath_2 = child_2.path[cut_point_1:cut_point_2 + 1]
    for i in range(cut_point_1, cut_point_2 + 1):
        if parent_2.path[i] in swath_1:
            continue
        else:
            v = parent_1.path[i]
            index = parent_2.path.index(v)
            while index in range(cut_point_1, cut_point_2 + 1):
                v = parent_1.path[index]
                index = parent_2.path.index(v)
            child_1.path[index] = parent_2.path[i]
            child_1.charging_times[index] = parent_2.charging_times[i]
    for i in range(cut_point_1, cut_point_2 + 1):
        if parent_1.path[i] in swath_2:
            continue
        else:
            v = parent_2.path[i]
            index = parent_1.path.index(v)
            while index in range(cut_point_1, cut_point_2 + 1):
                v = parent_2.path[index]
                index = parent_1.path.index(v)
            child_2.path[index] = parent_1.path[i]
            child_2.charging_times[index] = parent_1.charging_times[i]

    for i in it.chain(range(1, cut_point_1), range(cut_point_2 + 1, len(parent_1.path) - 1)):
        if child_1.path[i] == 0:
            child_1.path[i] = parent_2.path[i]
            child_1.charging_times[i] = parent_2.charging_times[i]
        if child_2.path[i] == 0:
            child_2.path[i] = parent_1.path[i]
            child_2.charging_times[i] = parent_1.charging_times[i]

    child_1 = init_fitness(child_1)
    child_2 = init_fitness(child_2)
    return child_1, child_2


def swap(child):
    swap_point_1 = np.random.randint(1, len(child.path) - 2)
    swap_point_2 = np.random.randint(1, len(child.path) - 2)
    while swap_point_2 == swap_point_1:
        swap_point_2 = np.random.randint(1, len(child.path) - 2)
    child.path[swap_point_1], child.path[swap_point_2] = child.path[swap_point_2], child.path[swap_point_1]
    child.charging_times[swap_point_1], child.charging_times[swap_point_2] = child.charging_times[swap_point_2], \
                                                                             child.charging_times[swap_point_1]

    return child


def reproduction_lower_level(chargingTimePopulation):
    for i in range(0, int(POP_SIZE / 2)):
        mom_1, dad_1 = np.random.randint(0, POP_SIZE - 1, 2)
        while dad_1 == mom_1:
            dad_1 = np.random.randint(0, POP_SIZE - 1)
        parent_1 = chargingTimePopulation.individuals[dad_1]
        parent_2 = chargingTimePopulation.individuals[mom_1]
        child_1 = Individual(parent_1)
        child_2 = Individual(parent_2)
        if np.random.rand() <= CROSSOVER_RATE:
            child_1, child_2 = spah(parent_1, parent_2)
            if np.random.rand() <= MUTATION_RATE:
                child_1 = mut_2(child_1)
            if np.random.rand() <= MUTATION_RATE:
                child_2 = mut_2(child_2)
        chargingTimePopulation.individuals.append(child_1)
        chargingTimePopulation.individuals.append(child_2)


def reproduction_upper_level(initPopulation):
    for i in range(0, int(POP_SIZE / 2)):
        mom_1, dad_1 = np.random.randint(0, POP_SIZE - 1, 2)
        while dad_1 == mom_1:
            dad_1 = np.random.randint(0, POP_SIZE - 1)
        parent_1 = initPopulation.individuals[dad_1]
        parent_2 = initPopulation.individuals[mom_1]
        child_1 = Individual(parent_1)
        child_2 = Individual(parent_2)
        if np.random.rand() <= CROSSOVER_RATE:
            child_1, child_2 = sbx(parent_1, parent_2)
            if np.random.rand() <= MUTATION_RATE:
                child_1 = polymute(child_1)
            if np.random.rand() <= MUTATION_RATE:
                child_2 = polymute(child_2)
        initPopulation.individuals.append(child_1)
        initPopulation.individuals.append(child_2)


def phase_bi_level():
    population_init_phase_2()
    for i in range(0, N_GENERATIONS_BI_LEVEL):
        print("BI LEVEL Gen " + str(i))
        upper_solutions = charging_path_solving_bi_level()
        lower_solutions = charging_time_solving_bi_level(upper_solutions)
        lower_solutions = sorted(lower_solutions, key=lambda x: x.fitness)
        for i in range(0, len(lower_solutions)):
            if population.individuals[i].fitness > lower_solutions[i].fitness:
                population.individuals[i] = lower_solutions[i]
        population.individuals.sort(key=lambda x: x.fitness)
        population.best = population.individuals[0]
    return population.best


def phase_3_deterministic():
    total_charging_time = np.min(np.array([E_MC - population.best.traveling_t * P_M, T - population.best.traveling_t]))
    remain_time = total_charging_time
    best_solution = population.best
    for j in range(1, len(best_solution.charging_times) - 1):
        sensor = sensors[best_solution.path[j]]
        time_upper_bound = remain_time
        if time_upper_bound > (E_MAX - E_MIN) / (U - sensor.p):
            t = (E_MIN - (sensor.remain_energy - T * sensor.p) + theta * (E_MAX - E_MIN)) / (U - sensor.p)
        if time_upper_bound < 0:
            t = 0
        best_solution.charging_times[j] = t
        remain_time -= t
    best_solution.charging_time = total_charging_time
    init_fitness(best_solution)
    return best_solution


if __name__ == '__main__':
    global T
    # get data file names from the data directory
    gc.enable()
    get_file_names(INPUT_DIRECTORY)
    # setup result file 
    fileghi1 = "B:\\code\\hflga\\HFLGA_code\\Bi-Level\\seed=1-10.txt";
    fileghi2 = "B:\\code\\hflga\\HFLGA_code\\Bi-Level\\thoigiandichuyenSeed=1-10.txt";
    fileghi3 = "B:\\code\\hflga\\HFLGA_code\\Bi-Level\\canduoi.txt";
    fileghi4 = "B:\\code\\hflga\\HFLGA_code\\Bi-Level\\cantren.txt";
    w_file = "B:\\code\\hflga\\HFLGA_code\\Bi-Level\\total_time_result.txt"
    # experiment iterations
    for i in range(0, 1):
        # loop for each data file
        for j in range(0, len(file_names)):
            for seed in range(0, n_seeds):
                for crate in CRATE:
                    U = crate
                    sensors = []
                    read_file(j, seed, 1.5)
                    T = 20000
                    time = T
                    start = timeit.default_timer()
                    while time <= total_T:
                        if total_T - time < T and total_T - time != 0:
                            T = total_T - time
                        phase_1()
                        solution = phase_2()
                        solution = phase_3()
                        # solution = phase_3_deterministic()
                        # solution = phase_bi_level()
                        check(solution)
                        print(solution.fitness)
                        print(solution.n_dead_nodes)
                        print("T = " + str(time) + "\n")
                        time += T
                    stop = timeit.default_timer()

                    write_file(fileghi1, fileghi2, fileghi3, fileghi4, w_file, j, stop - start, solution, crate)
